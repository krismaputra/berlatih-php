<?php
function tentukan_nilai($number)
{
    echo "<br>";
    //  kode disini
    if ($number >= 77 && $number <= 100 ){
        return "Sangat baik";
    }else if ($number >= 68 && $number <=76){
        return "Baik";
    }else if ($number >= 44 && $number <= 67){
        return "Cukup";
    }else{
        return "Kurang";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>